import Vue from 'vue'
import VueRouter from 'vue-router'
import Welcome from '../views/Welcome.vue'
import i18n from '../i18n'
import {db}  from '../firebase'

Vue.use(VueRouter)

const registeredRoutes = [
  'Welcome',
  'Home',
  'Administration',
  'UsersList',
  'ClassesList',
  'StudentRegistration',
  'WorkerRegistration',
  'CreateClass',
  'Profile',
  'UpdateProfile',
  'Class', 
  'UpdateClass'
];

const routes = [
  {
    path: '/',
    redirect: `/${i18n.locale}`
  },
  {
    path: '/:lang',
    component: {
      render (c) {return c('router-view')}
    },
    meta: {requiresAuth: false},
    children: [
      {
        path: '',
        name: 'Welcome',
        component: Welcome,
        meta: {requiresAuth: false},
      },
      {
        path: 'home',
        name: 'Home',
        component: () => import(/* webpackChunkName: "Home" */ '../views/Home.vue'),
        meta: {requiresAuth: true},
      },
      {
        path: 'administration',
        component: {
          render (c) {return c('router-view')}
        },
        children: [
          {
            path: '',
            name: 'Administration',
            component: () => import(/* webpackChunkName: "Administration" */ '../views/Administration.vue'),
            meta: {requiresAuth: true},
          },
          {
            path: 'all-users',
            name: 'UsersList',
            component: () => import(/* webpackChunkName: "UsersList" */ '../views/UsersList.vue'),
            meta: {requiresAuth: true},
          },
          {
            path: 'all-classes',
            name: 'ClassesList',
            component: () => import(/* webpackChunkName: "ClassesList" */ '../views/ClassesList.vue'),
            meta: {requiresAuth: true},
          },
          {
            path: 'registration/student',
            name: 'StudentRegistration',
            component: () => import(/* webpackChunkName: "StudentRegistration" */ '../views/StudentRegistration.vue'),
            meta: {requiresAuth: false},
          },
          {
            path: 'registration/worker',
            name: 'WorkerRegistration',
            component: () => import(/* webpackChunkName: "WorkerRegistration" */ '../views/WorkerRegistration.vue'),
            meta: {requiresAuth: true},
          },
          {
            path: 'create/class',
            name: 'CreateClass',
            component: () => import(/* webpackChunkName: "CreateClass" */ '../views/CreateClass.vue'),
            meta: {requiresAuth: true},
          },
        ],
      },
      {
        path: 'profile/:id',
        name: 'Profile',
        component: () => import(/* webpackChunkName: "Profile" */ '../views/Profile.vue'),
        meta: {requiresAuth: true},
      },
      {
        path: 'update-profile/:id',
        name: 'UpdateProfile',
        component: () => import(/* webpackChunkName: "UpdateProfile" */ '../views/UpdateProfile.vue'),
        meta: {requiresAuth: true},
      },
      {
        path: 'class/:id',
        name: 'Class',
        component: () => import(/* webpackChunkName: "Class" */ '../views/Class.vue'),
        meta: {requiresAuth: true},
      },
      {
        path: 'update-class/:id',
        name: 'UpdateClass',
        component: () => import(/* webpackChunkName: "UpdateClass" */ '../views/UpdateClass.vue'),
        meta: {requiresAuth: true},
      },
      {
        path: '404',
        alias: '*',
        name: 'NotFound',
        component: () => import(/* webpackChunkName: "NotFound" */ '../views/NotFound.vue'),
      },
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const dontExists = to.name in registeredRoutes;
  var language = to.params.lang;
  
  if(!language)
    language = 'sk';

  i18n.locale = language;

  if(dontExists)
    next({
      name: 'NotFound',
      params: {lang: i18n.locale}
    });
  else{
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const isAuthenticated = db.auth().currentUser;
    
    if(requiresAuth && isAuthenticated == null)
      next({
        name: 'Welcome',
        params: {lang: i18n.locale}
      });
    else
      next();
  }
})

export default router
