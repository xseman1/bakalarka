import firebase from 'firebase';
import 'firebase/auth';
import i18n from './i18n';
import router from './router';

const firebaseConfig = {
    apiKey: "AIzaSyApk92uZTLiWST9v3gwa7ZnYN4pA568atg",
    authDomain: "moja-jazykovka.firebaseapp.com",
    projectId: "moja-jazykovka",
    storageBucket: "moja-jazykovka.appspot.com",
    messagingSenderId: "886651050985",
    appId: "1:886651050985:web:c4fea54390958cb3e50978",
    measurementId: "G-54G8M9RNF0"
};

firebase.initializeApp(firebaseConfig);
firebase.analytics();
export const db = firebase;

 const tempUsers = [];
 const tempStudents = [];
 const tempLectors = [];
 const tempAdmins = [];
firebase.firestore().collection('users').onSnapshot((snapshot) => {
  const changes = snapshot.docChanges();
  
  const pushStudent = (data) => {
    tempStudents.push({
      id: data.doc.id,
      displayName: data.doc.data().name.first + ' ' + data.doc.data().name.last+ ' (' + data.doc.data().email + ')',
      ...data.doc.data()
    })
  };
  const pushLector = (data) => {
    tempLectors.push({
      id: data.doc.id,
      displayName: data.doc.data().name.first + ' ' + data.doc.data().name.last+ ' (' + data.doc.data().email + ')',
      ...data.doc.data()
    })
  };
  const pushAdmin = (data) => {
    tempAdmins.push({
      id: data.doc.id,
      displayName: data.doc.data().name.first + ' ' + data.doc.data().name.last+ ' (' + data.doc.data().email + ')',
      ...data.doc.data()
    })
  };
  const pushUser = (data) => {
    tempUsers.push({
      id: data.doc.id,
      displayName: data.doc.data().name.first + ' ' + data.doc.data().name.last,
      roleName: data.doc.data().role.isStudent ? i18n.t('firebase.student') : data.doc.data().role.isLector && !data.doc.data().role.isAdmin ? i18n.t('firebase.lector') : !data.doc.data().role.isLector && data.doc.data().role.isAdmin ? i18n.t('firebase.admin') : i18n.t('firebase.admin-lector'),
      ...data.doc.data()
    })
  };

  changes.forEach((change) => {
    if (change.type === "added") {
      if (change.doc.data().role.isStudent) pushStudent(change);
      if (change.doc.data().role.isLector) pushLector(change);
      if (change.doc.data().role.isAdmin) pushAdmin(change);
      pushUser(change);
    }
    else if(change.type === "modified") {
      if (change.doc.data().role.isStudent){
        for( var a = 0; a < tempStudents.length; a++){
          if(tempStudents[a].id === change.doc.id){
            tempStudents.splice(a, 1); 
            pushStudent(change);
          }
        }
      }

      if (change.doc.data().role.isLector){
        for( var b = 0; b < tempLectors.length; b++){
          if(tempLectors[b].id === change.doc.id){
            tempLectors.splice(b, 1); 
            pushLector(change);
          }
        }
      }

      if (change.doc.data().role.isAdmin){
        for( var c = 0; c < tempAdmins.length; c++){
          if(tempAdmins[c].id === change.doc.id){
            tempAdmins.splice(c, 1); 
            pushAdmin(change);
          }
        }
      }

      for( var x = 0; x < tempUsers.length; x++){
        if(tempUsers[x].id === change.doc.id){
          tempUsers.splice(x, 1); 
          pushUser(change);
        }
      }
    }
  });
});
export const users = tempUsers;
export const students = tempStudents;
export const lectors = tempLectors;
export const admins = tempAdmins;

const tempCourses = [];
firebase.firestore().collection('classes').onSnapshot((snapshot) => {
  const changes = snapshot.docChanges();

  changes.forEach((change) => {
    if(change.type === "added") {
      tempCourses.push({
        id: change.doc.id,
        ...change.doc.data()
      });
    }
    else if(change.type === "modified"){
      for( var x = 0; x < tempCourses.length; x++){
        if(tempCourses[x].id === change.doc.id){
          tempCourses.splice(x, 1);
          tempCourses.push({
            id: change.doc.id,
            ...change.doc.data()
          });
        }
      }
    }
  })  
});
export const courses = tempCourses;

export async function loginUser (data) {
  await firebase.auth().signInWithEmailAndPassword(data.email, data.password).then(() => {
    whoSigned();
    router.replace({
      name: "Home",
      params: {
        lang: i18n.locale,
        id: firebase.auth().currentUser.uid
      }
    })
  }).catch((error) => {
      switch (error.code) {
          case 'auth/user-not-found': 
            alert(i18n.t('alerts.user-not-found'));
            break;
          case 'auth/wrong-password': 
            alert(i18n.t('alerts.wrong-password')); 
            break;
          default: 
            alert(error.message)
      }
  });
}

export async function registerNewAccount(data) {

  //UPRAVIT. Ak sa registruje zamestanec nemozem sa prihlasit ak odany zamestanec
  await firebase.auth().createUserWithEmailAndPassword(data.user.email, data.password).then(() => {
    firebase.firestore().collection('users').doc(firebase.auth().currentUser.uid).set(data.user).then(() => {   
          alert(i18n.t('alerts.registration-ok'));
          router.replace({
            name: "Home",
            params: {
              lang: i18n.locale,
              id: firebase.auth().currentUser.uid
            }
          });
      }).catch((error) => {
        switch (error.code) {
          default: 
            alert(error.code + ': ' + error.message)
        }    
      });
    }).catch((error) => {
      switch (error.code) {
        case 'auth/email-already-in-use':
          alert(i18n.t('alerts.email-already-exists')); 
          break;
        default: 
          alert( error.code + ': ' + error.message)
      }   
  });
}

export async function createNewClass(data) {
  await firebase.firestore().collection('classes').add(data).then((course) => {
    alert(i18n.t('alerts.class-created'));

    data.students.forEach((student) => {
      firebase.firestore().collection('users').doc(student).update({
        courses: firebase.firestore.FieldValue.arrayUnion(course.id)
      })
    });

    firebase.firestore().collection('users').doc(data.lector).update({
      courses: firebase.firestore.FieldValue.arrayUnion(course.id)
    });

    router.replace({
      name: "Administration",
      params: {
        lang: i18n.locale,
        id: firebase.auth().currentUser.uid,
      }
    });
  }).catch((error) => {
    switch (error.code) {
      default: 
        alert( error.code + ': ' + error.message)
    }   
  });
}

export async function resetPassword (data) {
    await firebase.auth().sendPasswordResetEmail(data.email).then(() => {
      alert(i18n.t('alerts.reset-email-send'));   
    }).catch((error) => {
      switch (error.code) {
        case 'auth/invalid-sender':
          alert(i18n.t('alerts.sender-unknown'));
          break;
        case 'auth/too-many-requests':
            alert(i18n.t('alerts.unknown-activity'));
            break;
        default: 
          alert(error.code + ' ' + error.message)
      }
    });
}

export async function updateProfile (data) {
  await firebase.firestore().collection('users').doc(data.id).update(data.payload).then(() => {
    alert(i18n.t('alerts.update-successful'));   
    router.replace({
      name: 'Profile',
      params: {
          lang: i18n.locale,
          id: data.id
      }
    });  
  }).catch((error) => {
    alert(error.code + ' ' + error.message)
  });
}

export async function updateCoursesStudents (data) {
  await firebase.firestore().collection('classes').doc(data.id).update({
    students: data.payload
  }).then(() => {
    data.deleted.forEach((user) => {
      firebase.firestore().collection('users').doc(user).update({
        courses: firebase.firestore.FieldValue.arrayRemove(data.id)
      })
    });
    alert(i18n.t('alerts.update-class-successful'));
    router.replace({
      name: 'Class',
      params: {
          lang: i18n.locale,
          id: data.id
      }
    });  
  }).catch((error) => {
    alert(error.code + ' ' + error.message)
  });
}

var tempIsCurrentUserStudent = false;
var tempIsCurrentUserLector = false;
var tempIsCurrentUserAdmin = false;
async function whoSigned () {
  firebase.firestore().collection('users').doc(firebase.auth().currentUser.uid).get().then((user) => {
    tempIsCurrentUserStudent = user.data().role.isStudent;
    tempIsCurrentUserLector = user.data().role.isLector;
    tempIsCurrentUserAdmin = user.data().role.isAdmin;
  }).catch((error) => {
    alert(error.code + ': ' + error.message);
  });
}
export const isCurrentUserStudent = tempIsCurrentUserStudent;
export const isCurrentUserLector = tempIsCurrentUserLector;
export const isCurrentUserAdmin = tempIsCurrentUserAdmin;