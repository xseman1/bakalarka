import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#FFFFFF',
                secondary: '#363537',
                accent: '#0D2C54',
                error: '#9E0031',
                info: '#0267C1',
                success: '#95C623',
                warning: '#F26419',
            },
            dark: {
                primary: '#363537',
                secondary: '#FFFFFF',
                accent: '#0D2C54',
                error: '#9E0031',
                info: '#0267C1',
                success: '#95C623',
                warning: '#F26419',
            },
        },
    },
});
