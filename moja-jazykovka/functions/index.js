const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

exports.addStudentRole = functions.https.onCall((uid, context) => {
  return admin.auth().setCustomUserClaims(uid, {
    student: true,
    lector: false,
    admin: false,
  }).then(() => {
    return true;
  }).catch((error) => {
    return error;
  });
});

exports.addWorkerRole = functions.https.onCall((data, context) => {
  return admin.auth().setCustomUserClaims(data.uid, {
    student: false,
    lector: data.isLector,
    admin: data.isAdmin,
  }).then(() => {
    return true;
  }).catch((error) => {
    return error;
  });
});
